let counter = 1;
let zIndex = 1;

function renderCompany(company) {

    const main = document.querySelector(".main");
    main.innerHTML = ` `;
    main.innerHTML = `
<header class="header">
 <div class="header__item-name">
     <h2>Company Name : <span class="">${company.companyName}</span></h2>
 </div>
 <div class="header__item">
     <h5>Company Members : <span class="header__item-current">${company.curSize}</span></h5>
 </div>
 <div class="header__item">
     <h5>Company Members Maximum : <span class="header__item-max">${company.maxUsers}</span></h5>
 </div>
 </header>
 
 <table class="table">
 <thead class="thead-dark">
     <tr>
         <th scope="col">#</th>
         <th scope="col">Id</th>
         <th scope="col">Name</th>
         <th scope="col">Last Name</th>
         <th scope="col">Role</th>
         <th scope="col">Delete</th>
     </tr>
 </thead>
 <tbody class="table-body">
    
 </tbody>
 </table> `;
    deleteUserByButton();
};

function getCurrentLength() {
    let length = document.querySelector(".header__item-current");
    length.textContent = company.curSize;
}

function renderUser(user) {
    const table = document.querySelector(".table-body");
    const tr = document.createElement("tr");
    tr.innerHTML = `
    <th scope="col" class="user-${user.id}">${counter++}</th>
    <th scope="col">${user.id}</th>
    <th scope="col">${user.name}</th>
    <th scope="col">${user.lastName}</th>
    <th scope="col">${user.role}</th>
    <th scope="col" class="delete"><button type="button" data-id="${user.id}" class="btn btn-danger">Delete</butto</th>`;
    table.appendChild(tr);
    popUp(`User ${user.name} was created`);
};

function updateUser(user) {
    if (typeof user === "number") {
        let findUser = document.querySelector(`.user-${user}`);
        popUp(`User with id ${user} was deleted`);
        findUser.parentElement.remove();
    }
    if (typeof user === "object") {
        popUp(`User ${user.name} was updated`);
        let findUser = document.querySelector(`.user-${user.id}`);
        let text = findUser.textContent;
        findUser.parentNode.innerHTML = `
    <th scope="row" class=${'user-' + user.id}>${text}</th>
    <th scope="col">${user.id}</th>
    <th scope="col">${user.name}</th>
    <th scope="col">${user.lastName}</th>
    <th scope="col">${user.role}</th>
    <th scope="col" class="delete"><button type="button" data-id="${user.id}"data-id="${user.id}" class="btn btn-danger">Delete</butto</th>`;
    };
};


function popUp(action) {
    const pop = document.createElement("div");
    pop.innerHTML = `  <div class="modal-content" style="position:absolute;width: 300px; top: 90px; right: 10px; z-index:${zIndex++}">
    <div class="modal-header">
      <h5 class="modal-title">Company notify</h5>
    </div>
    <div class="modal-body">
      <p>${action}</p>
    </div>
  </div>`;
    let tableField = document.querySelector(".table");
    tableField.appendChild(pop);
    setTimeout(() => pop.remove(), 3000);
}


function deleteUserByButton() {
    let tableField = document.querySelector(".table");
    tableField.addEventListener("click", e => {
        let target = e.target;
        if (target.classList.contains("btn-danger")) {
            let buttonId = target.getAttribute("data-id");
            let elementParent = target.parentElement.parentElement;
            popUp(`User with id ${buttonId} was deleted`);
            superUser.deleteUser(buttonId);
            elementParent.remove();
        };
    });
}

