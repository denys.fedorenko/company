function main() {
    window.company = new Company("Denys International", 3);
    company.registerUserCreateCallback(user => renderUser(user));
    company.registerUserCreateCallback(() => getCurrentLength());
    company.registerUserUpdateCallback(() => getCurrentLength());
    company.registerUserUpdateCallback(user => updateUser(user));
    company.registerNoSpaceNotifyer(() => alert("This one was the last one"));

    renderCompany(company);

    window.superUser = Company.createSuperAdmin(company);
    window.den = superUser.createUser("Den", "Fedorenko", false, "Worker of the month");
    window.alex = superUser.createUser("Max", "Zorin", false, "Removed");
    window.hao = superUser.createUser("Max", "Zorin", false, "Removed");
}