var Company = (function () {
	const token = Symbol("token");
	const Token = "secret token";
	const registerUserCreateCallback = [];
	const registerUserUpdateCallback = [];
	const registerNoSpaceNotifyer = [];
	const companyMembers = Symbol("companyMembers");
	let id = 0;
	let password = Symbol("password");

	class User {
		#name;
		#lastName;
		#role;
		constructor(name, lastName, isAdmin = false, role) {
			this.#name = name;
			this.#lastName = lastName;
			this.id = id++;
			if (role) { this.#role = role; }
			if (isAdmin) {
				this.#role = "Super User";
				this[token] = Token;
				this.createUser = function (name, lastName, isAdmin = false, role) {
					if (this[password] != prompt("enter password please")) {
						throw "You are not autorizovan";
					}
					if (this[token] != Token) {
						throw "Your token is another";
					}
					let usersOfCompany = this.hisCompany[companyMembers];
					if (usersOfCompany.length === this.hisCompany.maxUsers) {
						throw ("error, no more space");
					}
					if (usersOfCompany.length === this.hisCompany.maxUsers - 1) {
						registerNoSpaceNotifyer.forEach(cb => cb('Its was the last one'));
					}
					if (usersOfCompany.length < this.hisCompany.maxUsers) {
						let newUser = new User(name, lastName, isAdmin = false, role);
						usersOfCompany.push(newUser);
						registerUserCreateCallback.forEach(el => el(newUser));
						return newUser;
					}
				}
			};
			this.deleteUser = function (id) {
				if (this[password] != prompt("enter password please")) {
					throw "You are not autorizovan";
				}
				if (this[token] != Token) {
					throw "Your token is another";
				}
				let usersOfCompany = this.hisCompany[companyMembers];
				let deleteFrom = usersOfCompany.findIndex(item => item.id === id);
				usersOfCompany.splice(deleteFrom, 1);
				registerUserUpdateCallback.forEach(cb => cb(id));

			};
		};

		get name() {
			return this.#name;
		};

		set name(newName) {
			this.#name = newName;
			registerUserUpdateCallback.forEach(cb => cb(this));
		};

		get lastName() {
			return this.#lastName;
		};

		set lastName(newLastname) {
			this.#lastName = newLastname;
			registerUserUpdateCallback.forEach(cb => cb(this));
		};

		get role() {
			return this.#role;
		};

		set role(newRole) {
			this.#role = newRole;
			registerUserUpdateCallback.forEach(cb => cb(this));
		};
	};

	return class Company {
		constructor(companyName, maxUsers) {
			this.companyName = companyName;
			this.maxUsers = maxUsers;
			this[companyMembers] = [];
		};

		getUser(id) {
			return this[companyMembers].find(item => item.id === id);
		};

		get curSize() {
			return this[companyMembers].length;
		}

		registerUserCreateCallback(callBack) {
			registerUserCreateCallback.push(callBack);
		};

		registerNoSpaceNotifyer(callBack) {
			registerNoSpaceNotifyer.push(callBack);
		};

		registerUserUpdateCallback(callBack) {
			registerUserUpdateCallback.push(callBack);
		};

		static createSuperAdmin(company) {
			if (company[companyMembers][0]) {
				throw "You have super user";
			}
			let superUser = new User("super", "user", true);
			superUser.hisCompany = company;
			company[companyMembers].unshift(superUser);
			superUser[password] = prompt("Enter your password");
			return superUser;

		};
	};
})()












